/* 
!   JSON Web Token

 *   JWT - It has certain properties that it holds. Likwise, JWT will hold values that we're going to specify, these are properties that we're going to send to the user that identifies if they are an admin or not.

*    JSON Web Token holds values and properties after authentication. For instance if you sign in to Facebook.com, once you've been authorised, that authentication is saved as a jsonwebtoken which identifies you as an authenticated user.
*/
//NOTE: Dependencies
const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI091!';

//Creating a function that "may" change. It is not a default function.
//It has a user params in order to get the users token that we want to mask
let createAcessToken = (user) => {
    console.log(user);

    //data - created to contain some user details that you want to create a token
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };

    console.log(data);

    //* sign() is a jwt method to create an access token. sign() is needed so that the token isn't tampered with. How it is created is how it should be used.
    //? first arg - data, what are the data's you want to create a token from
    //? second arg - secret, it's needed as it is similar to a signature where it signifies that the signature can only be used in a particular application
    //? third arg - option
    return jwt.sign(data, secret, {});
};

module.exports = {
    createAcessToken,
};

//NOTE:
/* 
    1. You can only get access token when a user logs in your app with the correct credentials

    2. As a user, you can only get your own details from your own token from logging in

    3. JWT is not meant for sensitive data.

    4. JWT is like a passport, you use around the app to access certain features meant for your type of user
*/