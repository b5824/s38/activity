/* 
    >> In courseRoutes.js:
    -Import express and save it in a variable called express.
    -Save the Router() method of express in a variable called router.
    -Import your courseControllers from your courseControllers file.
    -Create your route for course creation.

     >> Go back to your courseControllers.js file:
        -Import your Course model in the courseController.js file.
        -Add a new controller called addCourse which will allow us to add a course: 
        -Create a new course document out of your Course model.
        -Save your new course document.
            -Then send the result to the client.
            -Catch the error and send it to our client.
*/
const Course = require('../models/Course');

const addCourse = (req, res) => {
    //check if the course exists in the database
    //throw error if it does
    //save in the db if it doesn't
    let test = Course.estimatedDocumentCount();

    if (test) {}

    // Course.findOne({ name: req.body.name })
    //     .then((result) => {
    //         if (result.name === req.body.name) {
    //             return res.send('Course name already exists!');
    //         } else {
    //             let body = req.body;
    //             const newCourse = new Course({
    //                 name: body.name,
    //                 description: body.description,
    //                 price: body.price,
    //             });
    //             newCourse
    //                 .save()
    //                 .then((result) => res.send(result))
    //                 .catch((err) => res.send(err));
    //         }
    //     })
    //     .catch((err) => res.send(err));

    console.log(test);

    let body = req.body;
    const newCourse = new Course({
        name: body.name,
        description: body.description,
        price: body.price,
    });
    newCourse
        .save()
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

let getAllCourses = (req, res) => {
    Course.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

module.exports = {
    addCourse,
    getAllCourses,
};